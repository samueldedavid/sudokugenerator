// SudokuGenerator.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <ctime>
using namespace std;
typedef vector<int> Trow;
typedef vector<Trow> Tmatrix;
void printBoard( Tmatrix board);
bool checkBoard(Tmatrix board);
Tmatrix genBoard();
ostream& operator<<(ostream& os, const Tmatrix& board);;
ostream& operator<<(ostream& os, const Trow& row);
const int SIZE = 4;
int main()
{
	
	//Tmatrix board = vector<Trow>(SIZE , Trow(SIZE,-1));
	Tmatrix board[] = { 
	{
		{1,2,3,4},
		{3,4,1,2},
		{2,1,4,3},
		{4,3,2,1}
	},
	{
		{1,2,3,4},
		{1,2,3,4},
		{1,2,3,4},
		{1,2,3,4}
	} ,
	{
		{1,2,3,4},
		{4,3,2,1},
		{3,4,1,2},
		{4,3,2,1}
	}
	};
	Tmatrix m = genBoard();
	checkBoard(m);
	
	cout<< m;
	return 0;
}
Tmatrix genBoard() {
	/*
	There are divisions which must have 4 numbers
	which must not be in rows and columns 
	it is easiert to solve so 
	I do divisions first and then check the row and columns 
	of that entry

	*/


	Tmatrix rows= {
		{1,2,3,4},
		{1,2,3,4},
		{1,2,3,4},
		{1,2,3,4}
	};
	Tmatrix cols = {
		{1,2,3,4},
		{1,2,3,4},
		{1,2,3,4},
		{1,2,3,4}
	};
	Tmatrix blocks = {
		{1,2,3,4},
		{1,2,3,4},
		{1,2,3,4},
		{1,2,3,4}
	};
	//Tmatrix board(SIZE,Trow(SIZE,0));
	Tmatrix board{
		{1, 2, 3, 4},
		{ 5,6,7,8 },
		{ 9,10,11,12 },
		{ 13,14,15,16 }
	};

	/*I am attemting to solve it using blocks first and 
	then rows and cols and 
	bases of left over to fill the boxs*/
	int div = sqrt(SIZE);
	int step = SIZE / div;
	/*I move blocks first horizontal then next level vertically
						[0]|[1]
						[2]|[3]
	*/
	
	for (int blocky = 0; blocky < SIZE; blocky += step)
	{
		for (int blockx = 0; blockx < SIZE; blockx += step)
		{
			//cout << blocky << blockx << endl;
			for (int y = blocky; y < (blocky + div); y++)
			{
				Trow r = board[y];
				for (int x = blockx; x < (blockx + div); x++)
				{
					std::random_device rd;
					std::mt19937 g(time(0));
					// it can only do 24 permutations
					shuffle(blocks.back().begin(), blocks.back().end(),g);
					auto fill = blocks.back().back();
					cout<<blocks.back()<<endl;
					while (true)
					{
						if(
							end(cols[x]) != find(begin(cols[x]), end(cols[x]), fill) && 
						    end(rows[y]) != find(begin(rows[y]), end(rows[y]), fill)
						)
						{
							//cout<<"matched " ; 
							break;
						}else
						{
						//cout << "\nshuffling fill \n"<<fill << "\n"<<cols[x]<<"\n"<<rows[y];
						shuffle(blocks.back().begin(), blocks.back().end(), g);
						fill = blocks.back().back();
						}
					}
					cols[x].erase(remove(cols[x].begin(), cols[x].end(), fill), cols[x].end());
					rows[y].erase(remove(rows[y].begin(), rows[y].end(), fill), rows[y].end());
					board[y][x] = fill;
					

					blocks.back().pop_back();
					//cout << board[y][x];
				
				}
			}
			blocks.pop_back();
			//cout << "blocks size "<<blocks.size()<<endl;
		}
		
	

	}

	return board;
}
bool checkBoard(Tmatrix board) {
	cout<<"\nChecking divs\n";
	//Checking subdivision
	/*
		1 2#3 4
	    3 4#1 2
		#######
		2 1 4 3
		4 3 2 1
	 */
	
	int div = sqrt(SIZE);
	int step = SIZE / div;
	/*Start blocks row wise 
	idy is for row 
	idx is for column
	y is col index for block
	x is row index for block 
	*/
	for (int idy = 0; idy < SIZE; idy += step)
	{
		for (int idx = 0; idx < SIZE; idx += step)
		{
			vector<int> box;
			for (int y = idx; y < (idx + step); y++)
			{
				Trow row = board[y];
				//cout << row;
				
				for (int x = idy; x < (idy + step); x++)
				{
					//cout << row[x];
					auto result=find(begin(box),end(box), row[x]);
					if (result != end(box))
					{
						cout << "\nduplicates in " << x << y<<endl;
						return false;
					}
					else {
						box.push_back(row[x]);
					}
				}
				//cout << endl;
			}
			//cout << endl;
		}
	}

	cout<<"\ndone\nChecking rows and columns\n";
	for (int j = 0; j < SIZE; j++)
	{
		Trow row = board[j];
		vector<int> ro;
		for (int el : row)
		{
			auto result = find(begin(ro), end(ro), el);
			if (result != end(ro))
			{
				cout << "duplicates in row " << j << endl;
				cout << row;
				return false;
			}
			else {
				ro.push_back(el);
			}
		}
		vector<int> col;
		for (int i = 0; i < SIZE; i++)
		{
			auto result = find(begin(col), end(col), board[i][j]);
			if (result != end(col))
			{
				cout << "duplicates in col " << j << endl;
				return false;
			}
			else {
				col.push_back(board[i][j]);
			}

		}
	}
	cout<<"\nBoard OK\n";
	return true;
}
void printBoard(Tmatrix board) {
	for (Trow rows : board)
	{
		for (int i : rows)
			cout << i;
		cout << endl;
	}
}
//I just remembered that this can also be done and its is more natural
ostream& operator<<(ostream& os, const Tmatrix& board)
{
	for (Trow rows : board)
	{
		for (int i : rows)
			os << i << '|';
		os << endl;
	}
	
	return os;
}
ostream& operator<<(ostream& os, const Trow& row)
{
	for (int i : row)
			os << i << '|';
		os << endl;

	return os;
}

